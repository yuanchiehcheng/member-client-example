## 說明
此為泛科知識會員系統串接範例文件。  
可透過 nodejs@8 以上執行。

關鍵文件為：  
1. index.ejs -> 定義基本呼叫方式
2. assets/client.js -> 對應的client js script
3. server.js -> 多種呼叫範例
4. config.js -> 設定檔，記得根據環境而更動