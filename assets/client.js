window.onload = handle;

function handle() {
    document.getElementById("client-sniff").addEventListener('click', function () {
        axios.get(config.member.host + "api/server/sniff", {
            withCredentials: true
        }).then(result => {
            alert(result.data);
        }).catch(error => {
            console.error(error)
            alert(error);
        })
    })

    document.getElementById("client-ajax-login").addEventListener('click', function () {
        axios.post(config.member.host + "api/server/login", {
            identifier: config.member.test.account,
            password: config.member.test.password,
            client_id: config.member.client_id
        }, {
            withCredentials: true
        }).then(result => {
            alert(result.data.message.access_token);
        }).catch(error => {
            console.error(error)
            alert(error);
        })
    })

    document.getElementById("client-fb-login").addEventListener('click', function () {
        FB.login(function (response) {
            // 成功取得FB Token，送去伺服器驗證
            axios.post(config.member.host + "api/server/oauth/facebook", {
                access_token: response.authResponse.accessToken,
                client_id: config.member.client_id
            }, {
                withCredentials: true
            }).then(result => {
                alert(result.data.message.access_token);
            }).catch(error => {
                console.error(error);
                alert(error);
            })
        }, {
            scope: 'email, public_profile'
        })
    })

    document.getElementById("server-login").addEventListener('click', function () {
        // 從服務端登入
        axios.post("/member/ajax-login", {
            identifier: config.member.test.account,
            password: config.member.test.password
        }).then(result => {
            alert(result.data);
        }).catch(error => {
            console.error(error);
            alert(error);
        })
    })

    document.getElementById("server-fb-login").addEventListener('click', function () {
        FB.login(function (response) {
            // 成功取得FB Token，送去伺服器驗證
            axios.post("/member/ajax-oauth-login", {
                access_token: response.authResponse.accessToken,
                oauth_provider: 'facebook'
            }).then(result => {
                alert(result.data);
            }).catch(error => {
                console.error(error);
                alert(error);
            })
        }, {
            scope: 'email, public_profile'
        })
    })
}

// google 登入
function onSignIn(googleUser) {
    // Useful data for your client-side scripts:
    var profile = googleUser.getBasicProfile();
    var id_token = googleUser.getAuthResponse().id_token;
    axios.post(config.member.host + "api/server/oauth/google", {
        access_token: id_token,
        client_id: config.member.client_id
    }, {
        withCredentials: true
    }).then(result => {
        alert(result.data.message.access_token);
    }).catch(error => {
        console.error(error);
        alert(error);
    })
};