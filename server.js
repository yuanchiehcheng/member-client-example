const path = require('path');
const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static-server')
const render = require('koa-ejs');
const bodyParser = require('koa-bodyparser');

const axios = require('axios');

const config = require('./config')

const app = new Koa();
const router = new Router();

app.use(serve({
    rootDir: 'assets',
    rootPath: '/assets'
}));

render(app, {
    root: path.join(__dirname),
    layout: false,
    viewExt: 'ejs',
    cache: false,
    debug: false
});

app.use(bodyParser());
app.use(async (ctx,next) => {
    // the parsed body will store in ctx.request.body
    // if nothing was parsed, body will be an empty object {}
    ctx.body = ctx.request.body;
    await next();
});

router.get('/', async (ctx) => {
    await ctx.render('index', {
        config
    })
});

router.get('/success', async (ctx) => {
    await ctx.render('success')
});

// 會員系統 授權碼模式callback範例
router.get('/callback', async (ctx) => {
    try {
        // 透過 code 再去換token
        let code = ctx.query.code;
        if (!code) {
            throw Error('ServerError');
        }

        let response = await axios.post(config.member.host + "oauth/access_token", {
            code,
            client_id: config.member.client_id,
            client_secret: config.member.client_secret
        })

        let accessToken = 'Bearer ' + response.data.access_token;
        if (!accessToken) {
            return Promise.reject('ServerError');
        }

        // 用token取得用戶資料
        response = await axios.get(config.member.host + 'api/v1/profile', {
            headers: {
                authorization: accessToken,
            },
        });

        console.log("用戶資料", response.data);
        ctx.redirect("/success");
    } catch (err) {
        console.error(err)
    }
})

// 必須透過server-side取得用戶資料
router.post('/member/access-token', async (ctx) => {
    try{
        let response = await axios.get(config.member.host + 'api/v1/profile', {
            headers: {
                authorization: req.body.access_token,
            },
        });

        console.log("用戶資料", response.data);
        ctx.body = "取得用戶資料成功";
    }catch(err){

    }
})

router.post('/member/ajax-login', async (ctx) => {
    try {
        let response = await axios.post(config.member.host + "api/server/login", {
            identifier: config.member.test.account,
            password: config.member.test.password,
            client_id: config.member.client_id,
        })

        let accessToken = 'Bearer ' + response.data.message.access_token;
        if (!accessToken) {
            throw Error('ServerError');
        }

        // 用token取得用戶資料
        response = await axios.get(config.member.host + 'api/v1/profile', {
            headers: {
                authorization: accessToken,
            },
        });
        console.log("用戶資料", response.data);
        ctx.body = "login success";
    } catch (err) {
        // 如果是會員系統正常返回的錯誤訊息
        if (err.response) {
            ctx.status = err.response.status;
            ctx.body = {
                text: err.response.data.text
            };
        }
    }
})

router.post('/member/ajax-oauth-login', async (ctx) => {
    try {
        let oauth_provider = ctx.body.oauth_provider;
        let response = await axios.post(config.member.host + 'api/server/oauth/' + oauth_provider, {
            access_token: ctx.body.access_token,
            client_id: config.member.client_id,
        })

        let accessToken = 'Bearer ' + response.data.message.access_token;
        if (!accessToken) {
            throw Error('ServerError');
        }

        // 用token取得用戶資料
        response = await axios.get(config.member.host + 'api/v1/profile', {
            headers: {
                authorization: accessToken,
            },
        });
        console.log("用戶資料", response.data);
        ctx.body = "login success";
    } catch (err) {
        // 如果是會員系統正常返回的錯誤訊息
        console.log(err)
        if (err.response) {
            ctx.status = err.response.status;
            ctx.body = {
                text: err.response.data.text
            };
        }
    }
})

app.use(router.routes())
    .use(router.allowedMethods());


app.listen(config.port || 3000);